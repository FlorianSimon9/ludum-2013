using System.Collections;

using UnityEngine;

public class ThrowRandomBottle: MonoBehaviour {
    public  float       animationDuration    = 2.0f;
    public  float       fallCallbackDelay    = 0.5f;
    public  float       bellTopHeightOffset  = 5.0f;
    private bool        enableAnimation      = false;
    public  float       minDegreesPerSecond  = 270.0f;
    public  float       maxDegreesPerSecond  = 450.0f;
    public  float       animationMinPosition = 0.1f;
    public  Vector3     startOffset          = new Vector3(0.0f, 1.0f, 0.0f);

    private Vector3        to;
    private Vector3        from;
    private Transform      prefab;
    private float          elapsed;
    private Transform[]    prefabs;
    public  ParticleSystem explosion;
    private Vector3        rotationAxis;
    private AudioSource    shatteredSound;
    private float          degreesPerSecond;

    void Awake() {
        this.ensureComponentAssignedInEditor("Bottle Explosion", this.explosion);

        this.shatteredSound = this.getComponentWithAssertion<AudioSource>("Shattered bottle sound");

        this.prefabs = new Transform[transform.childCount];

        for (var i = 0; i < this.prefabs.Length; i++) {
            this.prefabs[i] = this.transform.GetChild(i);
        }
    }

    public void throwBottle(Vector3 to) {
        this.from = this.startOffset;
        this.to   = to;

        StartCoroutine(this.activateAnimation());
    }

    public IEnumerator activateAnimation() {
        this.elapsed          = 0.0f;
        this.enableAnimation  = true;
        this.prefab           = this.prefabs[Random.Range(0, this.prefabs.Length)];

        this.degreesPerSecond = Random.Range(this.minDegreesPerSecond, this.maxDegreesPerSecond);
        this.rotationAxis     = new Vector3(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f)).normalized;

        this.prefab.gameObject.SetActive(true);

        yield return new WaitForSeconds(this.animationDuration - this.fallCallbackDelay);

        GameState.instance.registerBottleFall(this.to);

        yield return new WaitForSeconds(this.fallCallbackDelay);

        this.explosion.transform.position = this.to;

        this.explosion.transform.gameObject.SetActive(false);
        this.explosion.transform.gameObject.SetActive(true);

        this.shatteredSound.Play();

        this.enableAnimation = false;

        this.prefab.gameObject.SetActive(false);

        yield return new WaitForSeconds(1.0f);

        this.explosion.Stop();
    }

    void FixedUpdate() {
        if (!this.enableAnimation) {
            return;
        }

        this.elapsed           += Time.fixedDeltaTime;

        var totalDegrees        = this.elapsed * this.degreesPerSecond;

        this.prefab.rotation    = Quaternion.AngleAxis(totalDegrees, this.rotationAxis);
        
        var animationPosition   = Mathf.Max(this.animationMinPosition, Mathf.Min(this.elapsed, animationDuration) / animationDuration);

        var position            = Vector3.Lerp(this.from, this.to, animationPosition);

        position.y             += bellTopHeightOffset * Mathf.Sin(animationPosition * Mathf.PI); // We want half a circle.

        this.prefab.position    = position;
    }
}
