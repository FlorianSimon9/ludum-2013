using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SliderFuelController : MonoBehaviour
{
    // Start is called before the first frame update
    int fuelToFill;


    public TextMeshProUGUI textmeshPro;
    public Slider slider;

    int value = 0;

    void Start()
    {
        fuelToFill = Constants.fuelToFill;
        textmeshPro.SetText("0/" + fuelToFill.ToString());
        slider.value = 0;
        slider.maxValue = fuelToFill;
    }

    public void AddValue()
    {
        value++;

        textmeshPro.SetText(value.ToString() + "/" + fuelToFill.ToString());
        slider.value = value;
    }

}
