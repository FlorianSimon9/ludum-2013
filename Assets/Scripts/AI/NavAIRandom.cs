using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class NavAIRandom : MonoBehaviour
{
    public UnityEngine.AI.NavMeshAgent agent;

    float minX = -Constants.worldBoxSide / 2 - 1.0f;

    float minY = -Constants.worldBoxSide / 2 - 1.0f;

    float maxX = Constants.worldBoxSide / 2;

    float maxY = Constants.worldBoxSide / 2 + 1.0f;

    public float waitMin = 0.5f;
    public float waitMax = 3f;
    
    public Vector3 destination;

    public bool moving = true;
    public bool isFrozen = false;
    Animator animator;
    AudioSource changeDirectionSound;

    void Start()
    {
        GameState.instance.bottleFall.AddListener(this.onBottleFall);

        changeDirectionSound = GetComponent<AudioSource>();

        waitMin = 0.5f;
        waitMax = 3f;

        destination = transform.position + 10 * Vector3.forward;

        animator = GetComponent<Animator>();

        this.startRandomMove();
    }

    float randomComponent(float min, float max) {
        var random = Random.Range(minX, maxX);

        // Yes this is biased, but I don't care.
        if (Mathf.Abs(random) < 3.0f) {
            return random + 20.0f;
        }

        return random;
    }

    void startRandomMove() {
        this.startMove(new Vector3(this.randomComponent(minX, maxX), 0, this.randomComponent(minY, maxY)));
    }

    void Update()
    {
        if (agent.hasPath && moving && Vector3.Distance(this.transform.position, destination) < 1.0f)
        {
            StartCoroutine(WaitAndStartMove());
        }
    }

    public void freeze() {
        StartCoroutine(this.pauseMovement(Constants.freezeDuration));
    }

    void unpause() {
        this.agent.isStopped = this.isFrozen = false;

        animator.SetFloat("Speed_f", 0.6f);
    }

    IEnumerator pauseMovement(float duration) {
        var velocity = this.agent.desiredVelocity;

        this.animator.SetFloat("Speed_f", 0.0f);

        this.agent.isStopped = this.isFrozen = true;

        yield return new WaitForSeconds(duration);

        this.unpause();
    }

    void startMove(Vector3 desiredDestination) {
        if (!this.agent.enabled) {
            return;
        }

        this.destination = desiredDestination;

        animator.SetFloat("Speed_f", 0.6f);

        NavMeshHit hit;
        if (NavMesh.SamplePosition(destination, out hit, 3.0f, NavMesh.AllAreas))
        {
            destination = hit.position;
            agent.SetDestination(hit.position);
        }
        else
        {
            Debug.LogError(destination);
            Debug.LogError(hit.position);
        }

        moving = true;
    }

    IEnumerator WaitAndStartMove()
    {
        moving = false;

        animator.SetFloat("Speed_f", 0.0f);

        yield return new WaitForSeconds(Random.Range(waitMin, waitMax));

        while (this.isFrozen) {
            yield return new WaitForSeconds(0.1f);
        }

        this.startRandomMove();
    }

    void onBottleFall(Vector3 fallPosition) {
        if (this.isFrozen || Vector3.Distance(fallPosition, this.transform.position) > Constants.bottleFearDistance || !this.agent.enabled) {
            return;
        }

        this.changeDirectionSound.Play();

        this.unpause();

        // Guaranteed to be off-limits.
        var runDirection = Constants.worldBoxSide * 3.0f * (this.transform.position - fallPosition);

        var scale = (Constants.worldBoxSide / 2.0f - 2.0f) / Mathf.Max(Mathf.Abs(runDirection.x), Mathf.Abs(runDirection.z));

        this.startMove((this.transform.position + runDirection) * scale);
    }
}
