using System;

using UnityEngine;

[Serializable]
public class Cooldown {
    private float timeLeft = 0.0f;
    public  float duration = 2.0f;

    public Cooldown(float duration) {
        this.duration = duration;
        this.timeLeft = 0.0f;
    }

    public bool isLocked {
        get {
            return this.timeLeft > 0.0f;
        }
    }

    public bool tryLocking() {
        if (this.isLocked) {
            return false;
        }

        this.timeLeft = this.duration;

        return true;
    }

    public void coolDown(float elapsedTime) {
        this.timeLeft = Mathf.Max(0.0f, this.timeLeft - elapsedTime);
    }
}
