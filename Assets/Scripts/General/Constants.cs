public static class Constants {
    public static readonly float  bottleFearDistance          = 5.0f;
    public static readonly float  cauldronFillingTime         = 10.0f;

    public static readonly string nonActionablePropsLayerName = "Non-Actionable Props";
    public static readonly string actionablePropsLayerName    = "ActionableProps";
    public static readonly string xThiefAxis                  = "Horizontal";
    public static readonly string shootableLayerName          = "Shootables";
    public static readonly string yThiefAxis                  = "Vertical";
    public static readonly float  worldBoxSide                = 60.0f;
   


    // GAMEPLAY VALUE :
    public static readonly float maxBottleThrowDuration = 1.5f;
    public static readonly float freezeDuration = 5.0f;
    public static readonly int countdownToSheriffGameplay = 2;

    public static readonly float bottleThrowCooldown = 3.0f;

    public static readonly int nbrCrowdDeadLost = 5;

    public static readonly int fuelToFill = 2;
    public static readonly float waitingTimeFillFuel = 2f;

    public static readonly int timeThiefEndGameSeconds = 4 * 60;
}
