using UnityEngine;

public static class Assertions {
    public static T getComponentWithAssertion<T>(this MonoBehaviour monoBehaviour, string name)
    where T: Component {
        var component = monoBehaviour.GetComponent<T>();

        if (!component) {
            ApplicationLifecycle.crash();

            throw new ComponentMissingError(name);
        }

        return component;
    }

    public static T getComponentWithAssertion<T>(this Transform transform, string name)
    where T: Component {
        var component = transform.GetComponent<T>();

        if (!component) {
            ApplicationLifecycle.crash();

            throw new ComponentMissingError(name);
        }

        return component;
    }

    public static void ensureGameObjectAssignedInEditor(this MonoBehaviour monoBehaviour, string name, GameObject dependency) {
        if (!dependency) {
            ApplicationLifecycle.crash();

            throw new MissingEditorDataError(name);
        }
    }

    public static void ensureTransformAssignedInEditor(this MonoBehaviour monoBehaviour, string name, Transform dependency) {
        if (!dependency) {
            ApplicationLifecycle.crash();

            throw new MissingEditorDataError(name);
        }
    }

    public static void ensureComponentAssignedInEditor(this MonoBehaviour monoBehaviour, string name, Component component) {
        if (!component) {
            ApplicationLifecycle.crash();

            throw new MissingEditorDataError(name);
        }
    }

    public static int getLayerByNameWithAssertion(this MonoBehaviour monoBehaviour, string name) {
        var layer = LayerMask.NameToLayer(name);

        if (layer == -1) {
            throw new InvalidLayerNameError(name);
        }

        return layer;
    }
}

public class MissingEditorDataError: System.Exception {
    public MissingEditorDataError(string name)
    : base($"Missing data that should have been assigned in the editor ({name})")
    {}
}

public class ComponentMissingError: System.Exception {
    public ComponentMissingError(string component)
    : base($"Cannot find an instance of the '{component}' component")
    {}
}

public class InvalidLayerNameError: System.Exception {
    public InvalidLayerNameError(string name)
    : base($"The '{name}' layer does not exist")
    {}
}
