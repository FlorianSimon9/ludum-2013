using UnityEngine;

public static class MathExtensions {
    public static float normalizeAngle(float angle) {
        if (angle >= 0.0f) {
            return angle % 360.0f;
        }

        return angle + 360.0f;
    }

    public static float get2dVectorAngle(Vector3 direction) {
        return MathExtensions.normalizeAngle(Mathf.Atan2(direction.z, direction.x) * Mathf.Rad2Deg);
    }
}
