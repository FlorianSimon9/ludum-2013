using UnityEngine;

public static class ApplicationLifecycle {
    public static void crash() {
        #if UNITY_EDITOR
            Time.timeScale = 0.0f;

            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }
}
