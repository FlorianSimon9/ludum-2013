using System.Collections;

using UnityEngine;

public enum CauldronState {
    Empty,
    Filling,
    Full
}

public class VodkaController: MonoBehaviour {
    private float waitingTime = Constants.cauldronFillingTime;

    public GameObject preparingVodkaVfx;
    public GameObject vodkaReadyVfx;

    public CauldronState state { get; private set; } = CauldronState.Empty;

    public void Awake() {
        this.disableVfx();
    }

    public void disableVfx() {
        this.preparingVodkaVfx.SetActive(false);
        this.vodkaReadyVfx.SetActive(false);
    }

    public void StartFilling() {
        this.state = CauldronState.Filling;

        this.preparingVodkaVfx.SetActive(true);

        StartCoroutine(VodkaFillingCoroutine());
    }

    public void EmptyCauldron() {
        this.state = CauldronState.Empty;

        this.disableVfx();
    }

    private void FinishFillingCauldron() {
        this.preparingVodkaVfx.SetActive(false);
        this.vodkaReadyVfx.SetActive(true);

        this.state = CauldronState.Full;
    }

    IEnumerator VodkaFillingCoroutine() {
        this.state = CauldronState.Filling;

        yield return new WaitForSeconds(waitingTime);

        this.FinishFillingCauldron();
    }
}
