using System.Collections.Generic;

using UnityEngine.XR;
using UnityEngine;

public enum Hand {
    Left,
    Right
}

public class VrInput {
    private InputDevice? leftHand = null;
    private InputDevice? rightHand = null;

    public bool isGripping(Hand hand) {
        return this.buttonIsPressed(hand, CommonUsages.gripButton);
    }

    public bool isPressingTrigger(Hand hand) {
        return this.buttonIsPressed(hand, CommonUsages.triggerButton);
    }

    public bool isPressingPrimaryButton(Hand hand) {
        return this.buttonIsPressed(hand, CommonUsages.primaryButton);
    }

    private bool buttonIsPressed(Hand hand, InputFeatureUsage<bool> usage) {
        bool pressed;

        var handDevice = this.getHand(hand);

        if (!handDevice.HasValue) {
            return false;
        }

        if (!handDevice.Value.TryGetFeatureValue(usage, out pressed)) {
            return false;
        }

        return pressed;
    }

    public Vector2 getJoystick(Hand hand) {
        Vector2 output;

        var handDevice = this.getHand(hand);

        if (!handDevice.HasValue || !handDevice.Value.TryGetFeatureValue(CommonUsages.primary2DAxis, out output)) {
            return Vector2.zero;
        }

        return output;
    }

    private InputDevice? getHand(Hand hand) {
        var isLeftHand = hand == Hand.Left;

        var characteristics = isLeftHand ? InputDeviceCharacteristics.Left : InputDeviceCharacteristics.Right;
        var inputDevice     = isLeftHand ? this.leftHand                   : this.rightHand;

        tryGettingFirstInputDevice(characteristics, ref inputDevice);

        if (isLeftHand) {
            this.leftHand = inputDevice;
        } else {
            this.rightHand = inputDevice;
        }

        return inputDevice;
    }

    private bool tryGettingFirstInputDevice(InputDeviceCharacteristics characteristics, ref InputDevice? inputDevice) {
        if (inputDevice?.isValid == true) {
            return true;
        }

        var devices = new List<InputDevice>();

        InputDevices.GetDevicesWithCharacteristics(characteristics, devices);

        if (devices.Count == 0) {
            inputDevice = null;

            return false;
        }

        inputDevice = devices[0];

        return true;
    }
}
