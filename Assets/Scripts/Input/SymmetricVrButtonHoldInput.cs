using System;

public enum SymmetricVrButtonHoldInputStateEvent {
    JustPressed,
    JustReleased
}

public struct SymmetricVrButtonHoldInputState {
    public SymmetricVrButtonHoldInputStateEvent? eventType;
    public float                                 holdRatio;

    public SymmetricVrButtonHoldInputState(SymmetricVrButtonHoldInputStateEvent? eventType, float ratio) {
        this.eventType = eventType;
        this.holdRatio = ratio;
    }
}

public class SymmetricVrButtonHoldInput {
    private VrInput input = new VrInput();

    public float holdMaxDuration = 0.0f;
    public float currentDuration = 0.0f;

    private bool isHolding {
        get {
            return this.currentDuration > 0.0f;
        }
    }

    public void reset() {
        this.currentDuration = 0.0f;
    }

    public SymmetricVrButtonHoldInput(float maxDuration) {
        this.holdMaxDuration = maxDuration;
    }

    public SymmetricVrButtonHoldInputState getState(float Δt) {
        var wasHolding    = this.isHolding;
        var buttonPressed = input.isPressingPrimaryButton(Hand.Left) || input.isPressingPrimaryButton(Hand.Right);

        SymmetricVrButtonHoldInputStateEvent? state = null;

        if (buttonPressed) {
            this.currentDuration = Math.Min(this.holdMaxDuration, this.currentDuration + Δt);
        } else {
            this.reset();
        }

        if (wasHolding != this.isHolding) {
            state = (
                wasHolding
                ? SymmetricVrButtonHoldInputStateEvent.JustReleased
                : SymmetricVrButtonHoldInputStateEvent.JustPressed
            );
        }

        return new SymmetricVrButtonHoldInputState(state, this.currentDuration / this.holdMaxDuration);
    }
}
