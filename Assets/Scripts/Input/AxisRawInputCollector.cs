using System.Linq;

using UnityEngine;

public struct Sample {
    public float value;
    public float measuredOver;

    public Sample(float value, float measuredOver) {
        this.value        = value;
        this.measuredOver = measuredOver;
    }
}

public class AxisRawInputCollector {
    private ushort   collectedSamplesThisTick = 0;
    private string   axisName;
    private Sample[] samples;

    public AxisRawInputCollector(string axisName) {
        this.samples  = new Sample[10];
        this.axisName = axisName;
    }

    public void sampleInUpdate(float elapsedTime) {
        if (this.collectedSamplesThisTick >= this.samples.Length) {
            return;
        }

        this.samples[this.collectedSamplesThisTick] = new Sample(this.getCurrentValue(), Time.deltaTime);

        this.collectedSamplesThisTick++;
    }

    public float consumeAveragedValueInFixedUpdate() {
        // In case two FixedUpdates happen before an Update.
        if (this.collectedSamplesThisTick == 0) {
            return this.getCurrentValue();
        }

        var validSamples  = this.samples.Take(this.collectedSamplesThisTick);

        var totalDuration = validSamples.Select(sample => sample.measuredOver).Sum();

        var average       = validSamples.Select(sample => sample.value * sample.measuredOver).Sum() / totalDuration;

        this.collectedSamplesThisTick = 0;

        return average;
    }

    private float getCurrentValue() {
        return Input.GetAxisRaw(this.axisName);
    }
}
