using UnityEngine;
using UnityEngine.Events;

public class GameState: MonoBehaviour {
    public bool sheriffCameraObscured = true;

    public UnityEvent          onGameEnd;
    public UnityEvent<Vector3> bottleFall;
    public UnityEvent          thiefVictory;
    public UnityEvent          sheriffVictory;
    public UnityEvent          carRefuelledSideEffects;

    int crowdHit = 0;

    bool gameEnded = false;

    public void registerCrowdHit() 
    {
        if (!gameEnded)
        {
            crowdHit++;
            if (Constants.nbrCrowdDeadLost == crowdHit)
            {
                this.thiefWins();
            }
        }
    }

    public void registerThiefHit() {
        this.sheriffWins();
    }

    public void registerBottleFall(Vector3 position) {
        this.bottleFall.Invoke(position);
    }

    public void sheriffWins() {
        gameEnded = true;
        this.sheriffVictory.Invoke();
        this.onGameEnd.Invoke();
    }

    public void thiefWins() {
        gameEnded = true;
        this.thiefVictory.Invoke();
        this.onGameEnd.Invoke();
    }

    public void registerCarRefuelled() {
        this.carRefuelledSideEffects.Invoke();
    }
 
    public static GameState instance { get; private set; }

    void Awake() {
        if (GameState.instance != null) {
            throw new System.Exception("The GameState singleton has been created twice. Do you have two game prefabs in your scene?");
        }

        GameState.instance = this;
    }
}
