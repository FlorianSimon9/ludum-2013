using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    public float waitingTime = 2f;

    public float speed = 20f;

    public bool waiting = false;
    public bool waitingDone = false;
    public bool endGame = false;

    public int currentLevelFuel = 0;
    public int levelFuelMax = 1;

    public SliderFuelController sliderCtrl;

    private IEnumerator coroutine;

    public void Start()
    {
        levelFuelMax = Constants.fuelToFill;
        waitingTime = Constants.waitingTimeFillFuel;
    }

    // Start is called before the first frame update
    public void Activate()
    {
        waiting = true;

        coroutine = FillFuelCoroutine();
        StartCoroutine(coroutine);
    }

    public void Deactive()
    {
        waiting = false;
        StopCoroutine(coroutine);
    }

    IEnumerator FillFuelCoroutine()
    {

        float f = 0.0f;

        while (f < waitingTime)
        {
            yield return new WaitForSeconds(0.1f);
            f = f + 0.1f;
        }

        currentLevelFuel++;
        if (currentLevelFuel == levelFuelMax)
        {
            StartCoroutine(StartVehicule());
            endGame = true;
        }

        if(sliderCtrl != null)
        sliderCtrl.AddValue();

        waitingDone = true;
        waiting = false;
    }

    IEnumerator StartVehicule()
    {

        float savedTime = 0f;

        while (true)
        {
            this.transform.Translate(Vector3.forward * speed * Time.deltaTime);
            savedTime += Time.deltaTime;

            if(savedTime > 3f)
                GameState.instance.thiefWins();

            yield return null;
        }
    }
    
}
