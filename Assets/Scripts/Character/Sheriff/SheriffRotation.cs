using UnityEngine;

public class SheriffRotation: MonoBehaviour {
    public Camera    vrCamera;
    public Transform sheriffMesh;

    void Awake() {
        this.ensureComponentAssignedInEditor("VR Camera",    this.vrCamera);
        this.ensureTransformAssignedInEditor("Sheriff mesh", this.sheriffMesh);
    }

    void Update() {
        this.sheriffMesh.transform.rotation = Quaternion.Euler(0.0f, vrCamera.transform.eulerAngles.y, 0.0f);
    }
}
