using System.Collections;

using UnityEngine;
using UnityEngine.UI;

public class CountdownToSheriffGameplay: MonoBehaviour {
    public Text     timerText;
    public Animator animator;

    void Awake() {
        this.ensureComponentAssignedInEditor("Countdown Timer Text", this.timerText);

        this.animator = this.getComponentWithAssertion<Animator>("Black Sphere Animator");
    }

    void Start() {
        StartCoroutine(this.countdown());
    }

    IEnumerator countdown() {
        for (var time = Constants.countdownToSheriffGameplay; time >= 1; time--) {
            this.timerText.text = time.ToString();

            // It's not a proper second-to-second countdown, but we don't give a flying elephant.
            yield return new WaitForSeconds(1.0f);
        }

        this.timerText.text = "";

        this.animator.Play("Reduce Alpha");

        GameState.instance.sheriffCameraObscured = false;
    }
}
