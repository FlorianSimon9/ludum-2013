using UnityEngine;

public class SheriffGun: MonoBehaviour {
    public Transform    leftSight;
    public Transform    rightSight;
    public GameObject   leftGunRendering;
    public AudioSource  freezeAudioSource;
    public GameObject   rightGunRendering;
    public AudioSource  leftGunshotAudioSource;
    public AudioSource  rightGunshotAudioSource;

    public CrowdFreeze  crowdFreeze;
    public LineRenderer leftShightThief;
    public LineRenderer rightShightThief;

    private int          shootableLayer;
    private LineRenderer leftSightRenderer;
    private int          nonActionableLayer;
    private LineRenderer rightSightRenderer;
    private int          actionablePropsLayer;

    public float    freezeAngle = 20.0f;
    private VrInput vrInput     = new VrInput();

    public Cooldown freezeCooldown;
    public Cooldown leftGunshotCooldown;
    public Cooldown rightGunshotCooldown;
    
    void Awake() {
        this.shootableLayer       = this.getLayerByNameWithAssertion(Constants.shootableLayerName);
        this.actionablePropsLayer = this.getLayerByNameWithAssertion(Constants.actionablePropsLayerName);
        this.nonActionableLayer   = this.getLayerByNameWithAssertion(Constants.nonActionablePropsLayerName);

        this.ensureComponentAssignedInEditor("Crowd freeze", this.crowdFreeze);
        this.ensureTransformAssignedInEditor("Sheriff Left Gun Sight", this.leftSight);
        this.ensureTransformAssignedInEditor("Sheriff Right Gun Sight", this.rightSight);
        this.ensureComponentAssignedInEditor("Freeze Audio Source", this.freezeAudioSource);
        this.ensureGameObjectAssignedInEditor("Sheriff Left Gun Rendering", this.leftGunRendering);
        this.ensureGameObjectAssignedInEditor("Sheriff Right Gun Rendering", this.rightGunRendering);
        this.ensureComponentAssignedInEditor("Left Gunshot Audio Source", this.leftGunshotAudioSource);
        this.ensureComponentAssignedInEditor("Right Gunshot Audio Source", this.rightGunshotAudioSource);

        this.leftSightRenderer  = this.leftSight.getComponentWithAssertion<LineRenderer>("Sheriff Left Gun Sight Line Renderer");
        this.rightSightRenderer = this.rightSight.getComponentWithAssertion<LineRenderer>("Sheriff Right Gun Sight Line Renderer");

        this.leftGunRendering.SetActive(false);
        this.rightGunRendering.SetActive(false);
    }

    void Update() {
        if (GameState.instance.sheriffCameraObscured) {
            return;
        }

        this.updateGun(Hand.Left,  this.leftSight,  this.leftGunRendering,  this.leftSightRenderer, leftShightThief, this.leftGunshotCooldown,  this.leftGunshotAudioSource);
        this.updateGun(Hand.Right, this.rightSight, this.rightGunRendering, this.rightSightRenderer, rightShightThief, this.rightGunshotCooldown, this.rightGunshotAudioSource);
    }

    private void handleGunshot(Cooldown gunshotCooldown, AudioSource gunshotAudioSource, bool somethingWasHit, Vector3 gunDirection, RaycastHit hit) {
        gunshotAudioSource.Play();

        if (somethingWasHit) {
            if (hit.collider.gameObject.tag == "Crowd") {

                if (hit.collider.GetComponent<RagdollController>() != null)
                {
                    Vector3 direction = hit.transform.position - this.transform.position;
                    
                    direction.Normalize();
                    direction = new Vector3(direction.x, 0.5f, direction.z);

                    hit.collider.GetComponent<RagdollController>().EnableRagdoll(direction, hit.point);
                }

                GameState.instance.registerCrowdHit();
            } else if (hit.collider.gameObject.tag == "Thief") {
                

                if (hit.collider.GetComponent<RagdollController>() != null)
                {
                    Vector3 direction = hit.transform.position - this.transform.position;

                    direction.Normalize();
                    direction = new Vector3(direction.x, 0.5f, direction.z);

                    hit.collider.GetComponent<RagdollController>().EnableRagdoll(direction* 2f, hit.point);
                }

                GameState.instance.registerThiefHit();
            }

            return;
        }

        var isFreezeAngle = Mathf.Abs(Vector3.Angle(gunDirection, Vector3.up)) < this.freezeAngle;

        if (!isFreezeAngle || !this.freezeCooldown.tryLocking()) {
            return;
        }

        this.freezeAudioSource.Play();

        this.crowdFreeze.freeze();
    }

    private void updateGun(
        Hand         hand,
        Transform    sight,
        GameObject   gunRendering,
        LineRenderer sightRenderer,
        LineRenderer sightRendererThief,
        Cooldown     gunshotCooldown,
        AudioSource  gunshotAudioSource
    ) {
        var gunIsActive  = this.vrInput.isGripping(hand);
        var wantsToShoot = this.vrInput.isPressingTrigger(hand);

        gunRendering.SetActive(gunIsActive);

        if (!gunIsActive) {
            return;
        }

        freezeCooldown.coolDown(Time.deltaTime);
        gunshotCooldown.coolDown(Time.deltaTime);

        var isShooting     = wantsToShoot && gunshotCooldown.tryLocking();

        var barrelPosition = sight.position;
        var maxLength      = Constants.worldBoxSide * 1.5f;

        var actualLength   = maxLength;

        RaycastHit hit;

        var lineRenderingDirection = new Vector3(0.0f, 0.0f, 1.0f);

        var worldGunDirection = gunRendering.transform.TransformDirection(lineRenderingDirection);

        var somethingWasHit = Physics.Raycast(
            new Ray(barrelPosition, worldGunDirection),
            out hit,
            maxLength,
            ~(this.shootableLayer | this.nonActionableLayer | this.actionablePropsLayer)
        );

        if (somethingWasHit) {
            actualLength = hit.distance;
        }

        sightRenderer.SetPosition(1, sightRenderer.GetPosition(0) + lineRenderingDirection * actualLength);
        sightRendererThief.SetPosition(1, sightRendererThief.GetPosition(0) + lineRenderingDirection * actualLength);

        if (isShooting) {
            this.handleGunshot(gunshotCooldown, gunshotAudioSource, somethingWasHit, worldGunDirection, hit);
        }
    }
}
