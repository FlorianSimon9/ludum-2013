using UnityEngine;

public class BottleThrows: MonoBehaviour {
    private  Vector3                    endPoint;
    private  Vector3                    startPoint;


    public   float                      startDistance       = 5.0f;
    public   Cooldown                   activeCyclesDelay   = new Cooldown(0.5f);
    public   Cooldown                   cannotThrowCooldown = new Cooldown(1.0f);
    public   float                      endDistance         = Constants.worldBoxSide / 2.0f - 1.0f;
    private  Cooldown                   bottleThrowCooldown = new Cooldown(Constants.bottleThrowCooldown);
    private  SymmetricVrButtonHoldInput vrInput             = new SymmetricVrButtonHoldInput(Constants.maxBottleThrowDuration);

    public   Transform                  marker;
    public   Transform                  vrCamera;
    private  SpriteRenderer             spriteRenderer;
    public   AudioSource                throwAudioSource;
    public   ThrowRandomBottle          throwRandomBottle;
    public   AudioSource                blockedAudioSource;

    void Awake() {
        this.ensureTransformAssignedInEditor("Marker", this.marker);
        this.ensureTransformAssignedInEditor("VR camera", this.vrCamera);
        this.ensureComponentAssignedInEditor("Throw Sound", this.throwAudioSource);
        this.ensureComponentAssignedInEditor("Bottle Thrower", this.throwRandomBottle);
        this.ensureComponentAssignedInEditor("Throw Block Sound", this.blockedAudioSource);

        this.spriteRenderer = this.marker.getComponentWithAssertion<SpriteRenderer>("Bottle Throw Marker Sprite");
    }

    void Update() {
        this.activeCyclesDelay.coolDown(Time.deltaTime);
        this.bottleThrowCooldown.coolDown(Time.deltaTime);
        this.cannotThrowCooldown.coolDown(Time.deltaTime);

        SymmetricVrButtonHoldInputState state = this.vrInput.getState(Time.deltaTime);

        var isHolding = state.holdRatio > 0.0f;

        if (this.activeCyclesDelay.isLocked || this.bottleThrowCooldown.isLocked) {
            this.vrInput.reset();

            if (isHolding && this.bottleThrowCooldown.isLocked && this.cannotThrowCooldown.tryLocking()) {
                this.blockedAudioSource.Play();
            }

            return;
        }

        if (state.eventType == SymmetricVrButtonHoldInputStateEvent.JustReleased) {
            this.throwBottle(state.holdRatio);

            return;
        }

        if (!isHolding || state.holdRatio == 1.0f) {
            if (isHolding) {
                this.activeCyclesDelay.tryLocking();
            }

            this.resetBottleThrow();

            return;
        }
        
        if (state.eventType == SymmetricVrButtonHoldInputStateEvent.JustPressed) {
            this.initializeNewThrow();
        }
        
        if (isHolding) {
            this.updateThrow(state.holdRatio);
        }
    }

    private void resetBottleThrow() {
        this.vrInput.reset();

        this.spriteRenderer.enabled = false;
        this.marker.position        = new Vector3(0.0f, this.marker.position.y, 0.0f);
    }

    private void initializeNewThrow() {
        this.spriteRenderer.enabled = true;

        var direction = (Quaternion.AngleAxis(this.vrCamera.eulerAngles.y, Vector3.up) * Vector3.forward).normalized;

        this.endPoint   = direction * this.endDistance;
        this.startPoint = direction * this.startDistance;
    }

    private void updateThrow(float ratio) {
        var position = Vector3.Lerp(this.startPoint, this.endPoint, ratio);

        position.y = this.marker.position.y;

        this.marker.position = position;

        this.marker.LookAt(new Vector3(0.0f, position.y, 0.0f));
    }

    private void throwBottle(float ratio) {
        this.bottleThrowCooldown.tryLocking();
        this.cannotThrowCooldown.tryLocking();

        this.throwAudioSource.Play();

        this.spriteRenderer.enabled = false;

        this.throwRandomBottle.throwBottle(
            new Vector3(this.marker.position.x, 0.0f, this.marker.position.z)
        );

        this.resetBottleThrow();
    }
}
