
using UnityEngine;
using UnityEngine.AI;

public class RagdollController : MonoBehaviour
{
    public Rigidbody rigidChest;
    public Transform ragdollTransform;
    public GameObject vfx;
    public Animator animator;
    private AudioSource killSound;

    public NavMeshObstacle obstacle;
    public ThiefMovement thiefMovement;
    public ThiefActionController actionCtrl;

    public NavMeshAgent agent;
    public NavAIRandom script;


    public BoxCollider colBox;
    public CapsuleCollider capsCol;

    public CapsuleCollider capsCol1;

    Rigidbody[] arrayRigiBody;
    Collider[] arrayCollider;

    // Start is called before the first frame update
    void Awake()
    {
        killSound = GetComponent<AudioSource>();
        arrayRigiBody = this.ragdollTransform.GetComponentsInChildren<Rigidbody>();
        arrayCollider = this.ragdollTransform.GetComponentsInChildren<Collider>();

        foreach (var rigid in arrayRigiBody)
        {
            rigid.detectCollisions = false;
            rigid.useGravity = false;
        }

        foreach (var col in arrayCollider)
        {
            col.enabled = false;
        }

        vfx.SetActive(false);
    }

    public void EnableRagdoll(Vector3 dir, Vector3 hitPosition)
    {
        foreach (var rigid in arrayRigiBody)
        {
            rigid.velocity = Vector3.zero;
            rigid.detectCollisions = true;
            rigid.useGravity = true;
        }

        foreach (var col in arrayCollider)
        {
            col.enabled = true;
        }

        animator.enabled = false;

        if(agent!= null)
            agent.enabled = false;

        if(script != null)
            script.enabled = false;

        if(colBox != null)
            colBox.enabled = false;

        capsCol.enabled = false;

        if(capsCol1 != null)
            capsCol1.enabled = false;

        if (obstacle != null)
            obstacle.enabled = false;

        if (thiefMovement != null)
            thiefMovement.enabled = false;

        if (actionCtrl != null)
            actionCtrl.enabled = false;

        rigidChest.AddForce(400f*dir, ForceMode.Impulse);

        killSound.Play();

        vfx.SetActive(true);
        vfx.transform.position = hitPosition;
        Destroy(vfx, 3f);
    }
}
