using UnityEngine;

public class CrowdFreeze : MonoBehaviour {
    private NavAIRandom[] bots;

    void Awake() {
        this.bots = GetComponentsInChildren<NavAIRandom>();
    }

    public void freeze() {
        foreach (var bot in this.bots) {
            bot.freeze();
        }
    }
}
