using UnityEngine;
using UnityEngine.UI;

public class ThiefCursorScript: MonoBehaviour {
    public  Transform characterPrefab;
    public  Camera    thiefCamera;
    private Image     image;
    public  Vector2   offset = new Vector2(-2.0f, 70.0f);

    void Start() {
        this.ensureTransformAssignedInEditor("Character prefab in thief cursor", this.characterPrefab);
        this.ensureComponentAssignedInEditor("Thief camera in thief cursor", this.thiefCamera);

        this.image = this.getComponentWithAssertion<Image>("Thief cursor image");
    }

    void Update() {
        this.image.enabled = true;

        image.rectTransform.anchoredPosition = this.thiefCamera.WorldToScreenPoint(characterPrefab.position);

        image.rectTransform.anchoredPosition += this.offset;
    }
}
