using UnityEngine;

public class ThiefMovement: MonoBehaviour {
    private AxisRawInputCollector xAxis = new AxisRawInputCollector(Constants.xThiefAxis);
    private AxisRawInputCollector yAxis = new AxisRawInputCollector(Constants.yThiefAxis);

    private Rigidbody rigidBody;
    public  Transform characterPrefab;
    private Animator  characterAnimator;  

    public  float unitsPerSecond               = 5.0f;
    private float characterPrefabAngleOffset   = 90.0f;

    void Awake() {
        this.rigidBody = this.getComponentWithAssertion<Rigidbody>("Thief rigidbody");

        this.ensureTransformAssignedInEditor("Thief mesh", this.characterPrefab);

        this.characterAnimator = this.characterPrefab.GetComponent<Animator>();
    }

    void Update() {
        xAxis.sampleInUpdate(Time.deltaTime);
        yAxis.sampleInUpdate(Time.deltaTime);
    }

    void FixedUpdate() {
        var direction = new Vector3(xAxis.consumeAveragedValueInFixedUpdate(), 0, yAxis.consumeAveragedValueInFixedUpdate()).normalized;

        var currentAngle = this.characterPrefab.rotation.y;

        if (direction != Vector3.zero) {
            this.characterPrefab.rotation = Quaternion.Euler(0.0f, -(MathExtensions.get2dVectorAngle(direction) - characterPrefabAngleOffset), 0.0f);

            this.characterAnimator.Play("Run_Static");

            this.characterAnimator.SetFloat("Speed_f", 1.0f);
        } else {
            this.characterAnimator.SetFloat("Speed_f", 0.0f);
        }

        this.rigidBody.velocity = direction * unitsPerSecond;
    }
}
