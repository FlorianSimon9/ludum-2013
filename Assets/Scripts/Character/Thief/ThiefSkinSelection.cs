using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThiefSkinSelection : MonoBehaviour
{
    public List<GameObject> listSkin = new List<GameObject>();

    // Start is called before the first frame update
    void Awake()
    {
        int val = Random.Range(0, listSkin.Count);

        for(int i = 0; i < listSkin.Count; i++)
        {
            listSkin[i].SetActive(i == val);
        }
    }

}
