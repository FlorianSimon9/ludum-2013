using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThiefActionController : MonoBehaviour
{
    public GameObject potato;
    public GameObject bottle;

    public Transform hand;

    public RectTransform thiefLoadingBar;
    public Camera thiefCamera;

    public LayerMask mask;

    int valMask = 0;

    bool handEmpty = true;
    bool isPotato = false;

    GameObject goHit;
    GameObject prop;

    float timeToWait = 0f;
    float savedTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        valMask = mask.value;
        thiefLoadingBar.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (!GameState.instance.sheriffCameraObscured)
        {
            RaycastHit hit;
            Vector3 pos = transform.position + Vector3.up * 1.5f;

            Debug.DrawRay(pos, transform.TransformDirection(Vector3.forward) * 2f, Color.red);

            if (Input.GetButton("joystick button 0") && Physics.Raycast(pos, transform.TransformDirection(Vector3.forward), out hit, 2f, valMask))
            {
                goHit = hit.transform.gameObject;

                var cartCtrl = goHit.GetComponent<CartPotatoController>();

                if (cartCtrl != null)
                {
                    if (!cartCtrl.waiting && handEmpty && !cartCtrl.waitingDone)
                    {
                        cartCtrl.Activate();

                        timeToWait = cartCtrl.waitingTime;
                        thiefLoadingBar.gameObject.SetActive(true);
                        thiefLoadingBar.anchoredPosition = this.thiefCamera.WorldToScreenPoint(hit.transform.position + Vector3.up * 4f);
                    }
                    else if (handEmpty && cartCtrl.waitingDone)
                    {
                        cartCtrl.waitingDone = false;
                        handEmpty = false;

                        prop = Instantiate(potato, hand);
                        prop.transform.localPosition = new Vector3(0.26f, 0f, 0f);
                        isPotato = true;
                    }
                }

                var vodkaCtrl = goHit.GetComponent<VodkaController>();

                if (vodkaCtrl != null)
                {
                    if (vodkaCtrl.state == CauldronState.Empty && !handEmpty && isPotato)
                    {
                        vodkaCtrl.StartFilling();

                        Destroy(prop);

                        handEmpty = true;
                    }
                    else if (handEmpty && vodkaCtrl.state == CauldronState.Full)
                    {
                        vodkaCtrl.EmptyCauldron();

                        handEmpty = false;

                        prop = Instantiate(bottle, hand);
                        prop.transform.localPosition = new Vector3(0.26f, 0f, 0f);
                        isPotato = false;
                    }
                }

                var carCtrl = goHit.GetComponent<CarController>();

                if (carCtrl != null)
                {
                    if (!carCtrl.waiting && (!handEmpty && !isPotato) && !carCtrl.waitingDone)
                    {
                        carCtrl.Activate();

                        timeToWait = carCtrl.waitingTime;
                        thiefLoadingBar.gameObject.SetActive(true);
                        thiefLoadingBar.anchoredPosition = this.thiefCamera.WorldToScreenPoint(hit.transform.position + Vector3.up * 4f);
                    }
                    else if ((!handEmpty && !isPotato) && carCtrl.waitingDone)
                    {
                        carCtrl.waitingDone = false;
                        handEmpty = true;

                        GameState.instance.registerCarRefuelled();

                        if (carCtrl.endGame)
                        {
                            this.transform.parent.gameObject.SetActive(false);
                        }

                        Destroy(prop);
                    }
                }

            }
            else if (goHit)
            {
                var cartCtrl = goHit.GetComponent<CartPotatoController>();
                if (cartCtrl != null && cartCtrl.waiting)
                {
                    cartCtrl.Deactive();
                    goHit = null;

                    timeToWait = 0f;
                    savedTime = 0f;
                    thiefLoadingBar.gameObject.SetActive(false);

                }
                else
                {
                    var carCtrl = goHit.GetComponent<CarController>();
                    if (carCtrl != null && carCtrl.waiting)
                    {
                        carCtrl.Deactive();
                        goHit = null;

                        timeToWait = 0f;
                        savedTime = 0f;
                        thiefLoadingBar.gameObject.SetActive(false);
                    }
                }

            }

            if (timeToWait != 0f)
            {
                savedTime += Time.fixedDeltaTime;
                RectTransform rect = (RectTransform)thiefLoadingBar.GetChild(0);
                rect.sizeDelta = new Vector2((savedTime * 80.0f) / timeToWait, rect.sizeDelta.y);

                if (savedTime >= timeToWait)
                {
                    timeToWait = 0f;
                    savedTime = 0f;
                    thiefLoadingBar.gameObject.SetActive(false);
                }

            }
        }
    }

}
