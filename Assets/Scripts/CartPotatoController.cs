using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartPotatoController : MonoBehaviour
{
    public float waitingTime = 2f;

    public bool waiting = false;
    public bool waitingDone = false;

    public List<GameObject> levelPotato = new List<GameObject>();

    int currentLevel = 0;

    private IEnumerator coroutine;

    private void Start()
    {
        currentLevel = levelPotato.Count;
    }

    // Start is called before the first frame update
    public void Activate()
    {
        waiting = true;

        coroutine = GetPotatoCoroutine();
        StartCoroutine(coroutine);
    }

    public void Deactive()
    {
        waiting = false;
        StopCoroutine(coroutine);
    }

    IEnumerator GetPotatoCoroutine()
    {

        float f = 0.0f;

        while (f < waitingTime)
        {
            yield return new WaitForSeconds(0.1f);
            f = f + 0.1f;
        }

        if (currentLevel != 0)
        {
            currentLevel--;
            levelPotato[currentLevel].SetActive(false);
        }

        waitingDone = true;
        waiting = false;
    }

}
