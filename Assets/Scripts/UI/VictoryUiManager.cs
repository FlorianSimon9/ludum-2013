using UnityEngine;
using UnityEngine.UI;

public class VictoryUiManager: MonoBehaviour {
    public Text thiefVictoryText;
    public Text sheriffVictoryText;

    void Awake() {
        this.ensureComponentAssignedInEditor("Victory screen thief victory text",   this.thiefVictoryText);
        this.ensureComponentAssignedInEditor("Victory screen sheriff victory text", this.sheriffVictoryText);

        this.thiefVictoryText.enabled   = false;
        this.sheriffVictoryText.enabled = false;
    }

    void Start() {
        GameState.instance.thiefVictory.AddListener(this.registerThiefVictory);
        GameState.instance.sheriffVictory.AddListener(this.registerSheriffVictory);
    }

    public void registerThiefVictory() {
        this.thiefVictoryText.enabled = true;
    }

    public void registerSheriffVictory() {
        this.sheriffVictoryText.enabled = true;
    }
}
