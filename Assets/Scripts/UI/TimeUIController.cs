using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimeUIController : MonoBehaviour
{
    public TextMeshProUGUI text;
    int time;
    int savedTime;

    int seconds;
    bool stopped = false;

    float timeF;
    // Start is called before the first frame update
    void Start()
    {
        GameState.instance.onGameEnd.AddListener(() => {
            this.stopped = true;
        });

        savedTime = time = Constants.timeThiefEndGameSeconds;
        timeF = (float)time;

        int minutes = time / 60;

        seconds = time - minutes * 60;

        string minutesStr;
        string secondsStr;

        if(minutes < 10)
        {
            minutesStr = "0" + minutes.ToString();
        }
        else
            minutesStr = minutes.ToString();

        if (seconds < 10)
        {
            secondsStr = "0" + seconds.ToString();
        }
        else
            secondsStr = seconds.ToString();

        text.SetText(minutesStr + " : " + secondsStr);
    }

    // Update is called once per frame
    void Update()
    {
        if (stopped) return;

        timeF = timeF - Time.deltaTime;
        time = (int)timeF;

        
        if (time != savedTime)
        {
            savedTime = time;

            int minutes = time / 60;

            seconds = time - minutes * 60;

            string minutesStr;
            string secondsStr;

            if (minutes < 10)
            {
                minutesStr = "0" + minutes.ToString();
            }
            else
                minutesStr = minutes.ToString();

            if (seconds < 10)
            {
                secondsStr = "0" + seconds.ToString();
            }
            else
                secondsStr = seconds.ToString();

            text.SetText(minutesStr + " : " + secondsStr);
        }

        if (time == 0)
        {
            this.enabled = false;
            GameState.instance.sheriffWins();
        }

    }
}
