using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PanelThiefUI : MonoBehaviour
{
    public GameObject dimed;

    public GameObject win;
    public GameObject lost;

    bool end = false;

    // Start is called before the first frame update
    void Start()
    {
        dimed.SetActive(false);
        GameState.instance.thiefVictory.AddListener(this.registerThiefVictory);
        GameState.instance.sheriffVictory.AddListener(this.registerSheriffVictory);
    }

    public void registerThiefVictory()
    {
        dimed.SetActive(true);
        win.SetActive(true);
       lost.SetActive(false);

        end = true;
    }

    public void registerSheriffVictory()
    {
        dimed.SetActive(true);
        win.SetActive(false);
        lost.SetActive(true);

        end = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (end)
        {
            if (Input.GetButton("joystick button 0"))
            {
                SceneManager.LoadScene(0, LoadSceneMode.Single);
            }
        }
    }
}
